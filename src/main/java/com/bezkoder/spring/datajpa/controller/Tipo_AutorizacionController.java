package com.bezkoder.spring.datajpa.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bezkoder.spring.datajpa.model.Tipo_Autorizacion;
import com.bezkoder.spring.datajpa.repository.Tipo_AutorizacionRepository;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api")

public class Tipo_AutorizacionController {

	@Autowired
	Tipo_AutorizacionRepository tipo_autorizacionRepository;

	@GetMapping("/tipo_autorizacion")
	public ResponseEntity<List<Tipo_Autorizacion>> getAllTipo_Autorizacion(@RequestParam(required = false) String nombre) {
		try {
			List<Tipo_Autorizacion> tipo_autorizacion = new ArrayList<Tipo_Autorizacion>();

			if (nombre == null)
				tipo_autorizacionRepository.findAll().forEach(tipo_autorizacion::add);
			else
				tipo_autorizacionRepository.findByNombreContaining(nombre).forEach(tipo_autorizacion::add);

			if (tipo_autorizacion.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(tipo_autorizacion, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/tipo_autorizacion/{id}")
	public ResponseEntity<Tipo_Autorizacion> getTipo_AutorizacionById(@PathVariable("id") long id) {
		Optional<Tipo_Autorizacion> Tipo_AutorizacionData = tipo_autorizacionRepository.findById(id);

		if (Tipo_AutorizacionData.isPresent()) {
			return new ResponseEntity<>(Tipo_AutorizacionData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/tipo_autorizacion")
	public ResponseEntity<Tipo_Autorizacion> createProceso(@RequestBody Tipo_Autorizacion tipo_autorizacion) {
		try {
			Tipo_Autorizacion _tipo_autorizacion = tipo_autorizacionRepository
					
			.save(new Tipo_Autorizacion(tipo_autorizacion.getIdtipo_autorizacion(), tipo_autorizacion.getNombre(), tipo_autorizacion.getDescripcion(), tipo_autorizacion.getEstado(), tipo_autorizacion.getFecha_alta(), tipo_autorizacion.getFecha_baja(), tipo_autorizacion.getFecha_modificacion(), tipo_autorizacion.getUsuario_alta(), tipo_autorizacion.getUsuario_baja(), tipo_autorizacion.getUsuario_modificacion()));
			
			return new ResponseEntity<>(_tipo_autorizacion, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping("/tipo_autorizacion/{id}")
	public ResponseEntity<Tipo_Autorizacion> updateSeccion(@PathVariable("id") long id, @RequestBody Tipo_Autorizacion tipo_autorizacion) {
		Optional<Tipo_Autorizacion> tipo_autorizacionData = tipo_autorizacionRepository.findById(id);

		if (tipo_autorizacionData.isPresent()) {
			Tipo_Autorizacion _tipo_autorizacion = tipo_autorizacionData.get();
			_tipo_autorizacion.setNombre(tipo_autorizacion.getNombre());
			_tipo_autorizacion.setDescripcion(tipo_autorizacion.getDescripcion());
			return new ResponseEntity<>(tipo_autorizacionRepository.save(_tipo_autorizacion), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/tipo_autorizacion/{id}")
	public ResponseEntity<HttpStatus> deleteTipo_Autorizacion(@PathVariable("id") long id) {
		try {
			tipo_autorizacionRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/tipo_autorizacion")
	public ResponseEntity<HttpStatus> deleteAllTipo_Autorizacion() {
		try {
			tipo_autorizacionRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
