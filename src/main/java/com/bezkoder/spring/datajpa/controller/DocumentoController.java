package com.bezkoder.spring.datajpa.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bezkoder.spring.datajpa.model.Documento;
import com.bezkoder.spring.datajpa.repository.DocumentoRepository;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api")
public class DocumentoController {

	@Autowired
	DocumentoRepository documentoRepository;

	@GetMapping("/documento")
	public ResponseEntity<List<Documento>> getAllDocumento(@RequestParam(required = false) String nombre) {
		try {
			List<Documento> documento = new ArrayList<Documento>();

			if (nombre == null)
				documentoRepository.findAll().forEach(documento::add);
			else
				documentoRepository.findByNombreContaining(nombre).forEach(documento::add);

			if (documento.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(documento, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/documento/{id}")
	public ResponseEntity<Documento> getTutorialById(@PathVariable("id") long id) {
		Optional<Documento> DocumentoData = documentoRepository.findById(id);

		if (DocumentoData.isPresent()) {
			return new ResponseEntity<>(DocumentoData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/documento")
	public ResponseEntity<Documento> createDocumento(@RequestBody Documento documento) {
		try {
			Documento _documento = documentoRepository
					
			.save(new Documento(documento.getNombre(), documento.getDescripcion(), documento.getEstado(), documento.getFecha_alta(), documento.getFecha_baja(), documento.getFecha_modificacion(), documento.getUsuario_alta(), documento.getUsuario_baja(), documento.getUsuario_modificacion()));
			
			return new ResponseEntity<>(_documento, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping("/documento/{id}")
	public ResponseEntity<Documento> updateTutorial(@PathVariable("id") long id, @RequestBody Documento documento) {
		Optional<Documento> documentoData = documentoRepository.findById(id);

		if (documentoData.isPresent()) {
			Documento _documento = documentoData.get();
			_documento.setNombre(documento.getNombre());
			_documento.setDescripcion(documento.getDescripcion());
			return new ResponseEntity<>(documentoRepository.save(_documento), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/sucursal/{id}")
	public ResponseEntity<HttpStatus> deleteDocumento(@PathVariable("id") long id) {
		try {
			documentoRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/sucursal")
	public ResponseEntity<HttpStatus> deleteAllDocumento() {
		try {
			documentoRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	
}
