package com.bezkoder.spring.datajpa.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bezkoder.spring.datajpa.model.Contacto;
import com.bezkoder.spring.datajpa.repository.ContactoRepository;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api")
public class ContactoController {

	@Autowired
	ContactoRepository tutorialRepository;

	@GetMapping("/contactos")
	public ResponseEntity<List<Contacto>> getAllTutorials(@RequestParam(required = false) String title) {
		try {
			List<Contacto> tutorials = new ArrayList<Contacto>();

			if (title == null)
				tutorialRepository.findAll().forEach(tutorials::add);
			else
				tutorialRepository.findByNombreContaining(title).forEach(tutorials::add);
			tutorialRepository.findByNombreContaining(title).forEach(tutorials::add);
			tutorialRepository.findByNombreContaining(title).forEach(tutorials::add);
				tutorialRepository.findByNombreContaining(title).forEach(tutorials::add);
				tutorialRepository.findByNombreContaining(title).forEach(tutorials::add);
				tutorialRepository.findByNombreContaining(title).forEach(tutorials::add);
				tutorialRepository.findByNombreContaining(title).forEach(tutorials::add);

			if (tutorials.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(tutorials, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/contacto/{id}")
	public ResponseEntity<Contacto> getTutorialById(@PathVariable("id") long id) {
		Optional<Contacto> tutorialData = tutorialRepository.findById(id);

		if (tutorialData.isPresent()) {
			return new ResponseEntity<>(tutorialData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/contactos")
	public ResponseEntity<Contacto> createTutorial(@RequestBody Contacto tutorial) {
		try {
			Contacto _tutorial = tutorialRepository
					.save(new Contacto(tutorial.getNombre(), tutorial.getDireccion()));
			return new ResponseEntity<>(_tutorial, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/contactos/{id}")
	public ResponseEntity<Contacto> updateTutorial(@PathVariable("id") long id, @RequestBody Contacto tutorial) {
		Optional<Contacto> tutorialData = tutorialRepository.findById(id);

		if (tutorialData.isPresent()) {
			Contacto _tutorial = tutorialData.get();
			_tutorial.setNombre(tutorial.getNombre());
			_tutorial.setDireccion(tutorial.getDireccion());
			return new ResponseEntity<>(tutorialRepository.save(_tutorial), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/contactos/{id}")
	public ResponseEntity<HttpStatus> deleteTutorial(@PathVariable("id") long id) {
		try {
			tutorialRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/contactos")
	public ResponseEntity<HttpStatus> deleteAllTutorials() {
		try {
			tutorialRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	
}
