package com.bezkoder.spring.datajpa.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bezkoder.spring.datajpa.model.Proceso;
import com.bezkoder.spring.datajpa.repository.ProcesoRepository;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api")
public class ProcesoController {

	@Autowired
	ProcesoRepository procesoRepository;

	@GetMapping("/proceso")
	public ResponseEntity<List<Proceso>> getAllProceso(@RequestParam(required = false) String nombre) {
		try {
			List<Proceso> proceso = new ArrayList<Proceso>();

			if (nombre == null)
				procesoRepository.findAll().forEach(proceso::add);
			else
				procesoRepository.findByNombreContaining(nombre).forEach(proceso::add);

			if (proceso.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(proceso, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/proceso/{id}")
	public ResponseEntity<Proceso> getProcesoById(@PathVariable("id") long id) {
		Optional<Proceso> ProcesoData = procesoRepository.findById(id);

		if (ProcesoData.isPresent()) {
			return new ResponseEntity<>(ProcesoData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/proceso")
	public ResponseEntity<Proceso> createProceso(@RequestBody Proceso proceso) {
		try {
			Proceso _proceso = procesoRepository
					
			.save(new Proceso(proceso.getIdseccion(), proceso.getNombre(), proceso.getDescripcion(), proceso.getEstado(), proceso.getFecha_alta(), proceso.getFecha_baja(), proceso.getFecha_modificacion(), proceso.getUsuario_alta(), proceso.getUsuario_baja(), proceso.getUsuario_modificacion()));
			
			return new ResponseEntity<>(_proceso, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping("/proceso/{id}")
	public ResponseEntity<Proceso> updateProceso(@PathVariable("id") long id, @RequestBody Proceso proceso) {
		Optional<Proceso> procesoData = procesoRepository.findById(id);

		if (procesoData.isPresent()) {
			Proceso _proceso = procesoData.get();
			_proceso.setNombre(proceso.getNombre());
			_proceso.setDescripcion(proceso.getDescripcion());
			return new ResponseEntity<>(procesoRepository.save(_proceso), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/proceso/{id}")
	public ResponseEntity<HttpStatus> deleteProceso(@PathVariable("id") long id) {
		try {
			procesoRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/proceso")
	public ResponseEntity<HttpStatus> deleteAllProceso() {
		try {
			procesoRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
