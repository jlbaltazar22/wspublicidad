package com.bezkoder.spring.datajpa.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bezkoder.spring.datajpa.model.Seccion;
import com.bezkoder.spring.datajpa.repository.SeccionRepository;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api")

public class SeccionController {

	@Autowired
	SeccionRepository seccionRepository;

	@GetMapping("/seccion")
	public ResponseEntity<List<Seccion>> getAllSeccion(@RequestParam(required = false) String nombre) {
		try {
			List<Seccion> seccion = new ArrayList<Seccion>();

			if (nombre == null)
				seccionRepository.findAll().forEach(seccion::add);
			else
				seccionRepository.findByNombreContaining(nombre).forEach(seccion::add);

			if (seccion.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(seccion, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/seccion/{id}")
	public ResponseEntity<Seccion> getSeccionById(@PathVariable("id") long id) {
		Optional<Seccion> SeccionData = seccionRepository.findById(id);

		if (SeccionData.isPresent()) {
			return new ResponseEntity<>(SeccionData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/seccion")
	public ResponseEntity<Seccion> createProceso(@RequestBody Seccion seccion) {
		try {
			Seccion _seccion = seccionRepository
					
			.save(new Seccion(seccion.getIdseccion(), seccion.getNombre(), seccion.getDescripcion(), seccion.getEstado(), seccion.getFecha_alta(), seccion.getFecha_baja(), seccion.getFecha_modificacion(), seccion.getUsuario_alta(), seccion.getUsuario_baja(), seccion.getUsuario_modificacion()));
			
			return new ResponseEntity<>(_seccion, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping("/seccion/{id}")
	public ResponseEntity<Seccion> updateSeccion(@PathVariable("id") long id, @RequestBody Seccion seccion) {
		Optional<Seccion> seccionData = seccionRepository.findById(id);

		if (seccionData.isPresent()) {
			Seccion _seccion = seccionData.get();
			_seccion.setNombre(seccion.getNombre());
			_seccion.setDescripcion(seccion.getDescripcion());
			return new ResponseEntity<>(seccionRepository.save(_seccion), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/seccion/{id}")
	public ResponseEntity<HttpStatus> deleteSeccion(@PathVariable("id") long id) {
		try {
			seccionRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/seccion")
	public ResponseEntity<HttpStatus> deleteAllSeccion() {
		try {
			seccionRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
