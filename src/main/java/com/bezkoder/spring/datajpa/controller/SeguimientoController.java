package com.bezkoder.spring.datajpa.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bezkoder.spring.datajpa.model.Seguimiento;
import com.bezkoder.spring.datajpa.repository.SeguimientoRepository;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api")

public class SeguimientoController {

	@Autowired
	SeguimientoRepository seguimientoRepository;

	@GetMapping("/seguimiento")
	public ResponseEntity<List<Seguimiento>> getAllSeguimiento(@RequestParam(required = false) String usuario_alta) {
		try {
			List<Seguimiento> seguimiento = new ArrayList<Seguimiento>();

			//if (usuario_alta == null)
				seguimientoRepository.findAll().forEach(seguimiento::add);
			//else
			//	seguimientoRepository.findByUsuario_altaContaining(usuario_alta).forEach(seguimiento::add);

			if (seguimiento.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(seguimiento, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/seguimiento/{id}")
	public ResponseEntity<Seguimiento> getSeguimientoById(@PathVariable("id") long id) {
		Optional<Seguimiento> SeguimientoData = seguimientoRepository.findById(id);

		if (SeguimientoData.isPresent()) {
			return new ResponseEntity<>(SeguimientoData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/seguimiento")
	public ResponseEntity<Seguimiento> createProceso(@RequestBody Seguimiento seguimiento) {
		try {
			Seguimiento _seguimiento = seguimientoRepository
					
			.save(new Seguimiento(seguimiento.getIdtramite(), seguimiento.getIdtarea(), seguimiento.getAccion_realizada(), seguimiento.getIdestado_seguimiento(), seguimiento.getFecha_inicio(), seguimiento.getFecha_fin(), seguimiento.getIdseguimiento_anterior(), seguimiento.getIdusuario_responsable(), seguimiento.getTiempo_espera(), seguimiento.getTiempo_atencion(), seguimiento.getEstado(), seguimiento.getFecha_alta(), seguimiento.getFecha_baja(), seguimiento.getFecha_modificacion(), seguimiento.getUsuario_alta(), seguimiento.getUsuario_baja(), seguimiento.getUsuario_modificacion()));
			
			return new ResponseEntity<>(_seguimiento, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping("/seguimiento/{id}")
	public ResponseEntity<Seguimiento> updateSeguimiento(@PathVariable("id") long id, @RequestBody Seguimiento seguimiento) {
		Optional<Seguimiento> seguimientoData = seguimientoRepository.findById(id);

		if (seguimientoData.isPresent()) {
			Seguimiento _seguimiento = seguimientoData.get();
			_seguimiento.setIdtarea(seguimiento.getIdtarea());
			_seguimiento.setAccion_realizada(seguimiento.getAccion_realizada());
			return new ResponseEntity<>(seguimientoRepository.save(_seguimiento), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/seguimiento/{id}")
	public ResponseEntity<HttpStatus> deleteSeguimiento(@PathVariable("id") long id) {
		try {
			seguimientoRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/seguimiento")
	public ResponseEntity<HttpStatus> deleteAllSeguimiento() {
		try {
			seguimientoRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
