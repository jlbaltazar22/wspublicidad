package com.bezkoder.spring.datajpa.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bezkoder.spring.datajpa.model.Proceso_Tarea;
import com.bezkoder.spring.datajpa.repository.Proceso_TareaRepository;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api")
public class Proceso_TareaController {

	@Autowired
	Proceso_TareaRepository proceso_tareaRepository;

	@GetMapping("/proceso_tarea")
	public ResponseEntity<List<Proceso_Tarea>> getAllProceso_Tarea(@RequestParam(required = false) String usuario_alta) {
		try {
			List<Proceso_Tarea> proceso_tarea = new ArrayList<Proceso_Tarea>();

			//if (nombre == null)
				proceso_tareaRepository.findAll().forEach(proceso_tarea::add);
			//else
			//	proceso_tareaRepository.findByNombreContaining(nombre).forEach(estado_tramite::add);

			if (proceso_tarea.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(proceso_tarea, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	
	}

	@GetMapping("/proceso_tarea/{id}")
	public ResponseEntity<Proceso_Tarea> getProceso_TareaById(@PathVariable("id") long id) {
		Optional<Proceso_Tarea> Proceso_TareaData = proceso_tareaRepository.findById(id);

		if (Proceso_TareaData.isPresent()) {
			return new ResponseEntity<>(Proceso_TareaData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/proceso_tarea")
	public ResponseEntity<Proceso_Tarea> createProceso_Tarea(@RequestBody Proceso_Tarea proceso_tarea) {
		try {
			Proceso_Tarea _proceso_tarea = proceso_tareaRepository
					
			.save(new Proceso_Tarea(proceso_tarea.getIdproceso(), proceso_tarea.getIdtarea(), proceso_tarea.getSecuencia(), proceso_tarea.getEstado(), proceso_tarea.getFecha_alta(), proceso_tarea.getFecha_baja(), proceso_tarea.getFecha_modificacion(), proceso_tarea.getUsuario_alta(), proceso_tarea.getUsuario_baja(), proceso_tarea.getUsuario_modificacion()));
			
			return new ResponseEntity<>(_proceso_tarea, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping("/proceso_tarea/{id}")
	public ResponseEntity<Proceso_Tarea> updateProceso_Tarea(@PathVariable("id") long id, @RequestBody Proceso_Tarea proceso_tarea) {
		Optional<Proceso_Tarea> proceso_tareaData = proceso_tareaRepository.findById(id);

		if (proceso_tareaData.isPresent()) {
			Proceso_Tarea _proceso_tarea = proceso_tareaData.get();
			_proceso_tarea.setUsuario_alta(proceso_tarea.getUsuario_alta());
			_proceso_tarea.setUsuario_baja(proceso_tarea.getUsuario_baja());
			return new ResponseEntity<>(proceso_tareaRepository.save(_proceso_tarea), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/proceso_tarea/{id}")
	public ResponseEntity<HttpStatus> deleteProceso_Tarea(@PathVariable("id") long id) {
		try {
			proceso_tareaRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/proceso_tarea")
	public ResponseEntity<HttpStatus> deleteAllProceso_Estado() {
		try {
			proceso_tareaRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
}
