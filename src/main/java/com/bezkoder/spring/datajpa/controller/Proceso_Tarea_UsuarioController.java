package com.bezkoder.spring.datajpa.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bezkoder.spring.datajpa.model.Proceso_Tarea_Usuario;
import com.bezkoder.spring.datajpa.repository.Proceso_Tarea_UsuarioRepository;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api")
public class Proceso_Tarea_UsuarioController {

	@Autowired
	Proceso_Tarea_UsuarioRepository proceso_tarea_usuarioRepository;

	@GetMapping("/proceso_tarea_usuario")
	public ResponseEntity<List<Proceso_Tarea_Usuario>> getAllProceso_Tarea_Usuario(@RequestParam(required = false) String usuario_alta) {
		try {
			List<Proceso_Tarea_Usuario> proceso_tarea_usuario = new ArrayList<Proceso_Tarea_Usuario>();

			//if (nombre == null)
				proceso_tarea_usuarioRepository.findAll().forEach(proceso_tarea_usuario::add);
			//else
			//	proceso_tareaRepository.findByNombreContaining(nombre).forEach(estado_tramite::add);

			if (proceso_tarea_usuario.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(proceso_tarea_usuario, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	
	}

	@GetMapping("/proceso_tarea_usuario/{id}")
	public ResponseEntity<Proceso_Tarea_Usuario> getProceso_Tarea_UsuarioById(@PathVariable("id") long id) {
		Optional<Proceso_Tarea_Usuario> Proceso_Tarea_UsuarioData = proceso_tarea_usuarioRepository.findById(id);

		if (Proceso_Tarea_UsuarioData.isPresent()) {
			return new ResponseEntity<>(Proceso_Tarea_UsuarioData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/proceso_tarea_usuario")
	public ResponseEntity<Proceso_Tarea_Usuario> createProceso_Tarea_Usuario(@RequestBody Proceso_Tarea_Usuario proceso_tarea_usuario) {
		try {
			Proceso_Tarea_Usuario _proceso_tarea_usuario = proceso_tarea_usuarioRepository
					
			.save(new Proceso_Tarea_Usuario(proceso_tarea_usuario.getIdproceso(), proceso_tarea_usuario.getIdtarea(), proceso_tarea_usuario.getIdpersona(), proceso_tarea_usuario.getEstado(), proceso_tarea_usuario.getFecha_alta(), proceso_tarea_usuario.getFecha_baja(), proceso_tarea_usuario.getFecha_modificacion(), proceso_tarea_usuario.getUsuario_alta(), proceso_tarea_usuario.getUsuario_baja(), proceso_tarea_usuario.getUsuario_modificacion()));
			
			return new ResponseEntity<>(_proceso_tarea_usuario, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping("/proceso_tarea_usuario/{id}")
	public ResponseEntity<Proceso_Tarea_Usuario> updateProceso_Tarea_Usuario(@PathVariable("id") long id, @RequestBody Proceso_Tarea_Usuario proceso_tarea_usuario) {
		Optional<Proceso_Tarea_Usuario> proceso_tarea_usuarioData = proceso_tarea_usuarioRepository.findById(id);

		if (proceso_tarea_usuarioData.isPresent()) {
			Proceso_Tarea_Usuario _proceso_tarea_usuario = proceso_tarea_usuarioData.get();
			_proceso_tarea_usuario.setUsuario_alta(proceso_tarea_usuario.getUsuario_alta());
			_proceso_tarea_usuario.setUsuario_baja(proceso_tarea_usuario.getUsuario_baja());
			return new ResponseEntity<>(proceso_tarea_usuarioRepository.save(_proceso_tarea_usuario), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/proceso_tarea_usuario/{id}")
	public ResponseEntity<HttpStatus> deleteProceso_Tarea_Usuario(@PathVariable("id") long id) {
		try {
			proceso_tarea_usuarioRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/proceso_tarea_usuario")
	public ResponseEntity<HttpStatus> deleteAllProceso_Tarea_Usuario() {
		try {
			proceso_tarea_usuarioRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
}
