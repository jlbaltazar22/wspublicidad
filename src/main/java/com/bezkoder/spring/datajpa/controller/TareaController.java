package com.bezkoder.spring.datajpa.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bezkoder.spring.datajpa.model.Tarea;
import com.bezkoder.spring.datajpa.repository.TareaRepository;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api")

public class TareaController {

	@Autowired
	TareaRepository tareaRepository;

	@GetMapping("/tarea")
	public ResponseEntity<List<Tarea>> getAllTarea(@RequestParam(required = false) String nombre) {
		try {
			List<Tarea> tarea = new ArrayList<Tarea>();

			if (nombre == null)
				tareaRepository.findAll().forEach(tarea::add);
			else
				tareaRepository.findByNombreContaining(nombre).forEach(tarea::add);

			if (tarea.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(tarea, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/tarea/{id}")
	public ResponseEntity<Tarea> getTareaById(@PathVariable("id") long id) {
		Optional<Tarea> TareaData = tareaRepository.findById(id);

		if (TareaData.isPresent()) {
			return new ResponseEntity<>(TareaData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/tarea")
	public ResponseEntity<Tarea> createProceso(@RequestBody Tarea tarea) {
		try {
			Tarea _tarea = tareaRepository
					
			.save(new Tarea(tarea.getNotificacion_correo(), tarea.getRequiere_autorizacion(), tarea.getNotificacion_autorizador(), tarea.getIdtipo_autorizacion(), tarea.getUrl_formulario(), tarea.getIdproceso(), tarea.getEstado(), tarea.getFecha_alta(), tarea.getFecha_baja(), tarea.getFecha_modificacion(), tarea.getUsuario_alta(), tarea.getUsuario_baja(), tarea.getUsuario_modificacion(), tarea.getNombre(), tarea.getDescripcion(), tarea.getDuracion_estandar(), tarea.getDuracion_media(), tarea.getDuracion_alta()));
			
			return new ResponseEntity<>(_tarea, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping("/tarea/{id}")
	public ResponseEntity<Tarea> updateSeccion(@PathVariable("id") long id, @RequestBody Tarea seccion) {
		Optional<Tarea> tareaData = tareaRepository.findById(id);

		if (tareaData.isPresent()) {
			Tarea _tarea = tareaData.get();
			_tarea.setNombre(seccion.getNombre());
			_tarea.setDescripcion(seccion.getDescripcion());
			return new ResponseEntity<>(tareaRepository.save(_tarea), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/tarea/{id}")
	public ResponseEntity<HttpStatus> deleteTarea(@PathVariable("id") long id) {
		try {
			tareaRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/tarea")
	public ResponseEntity<HttpStatus> deleteAllTarea() {
		try {
			tareaRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
