package com.bezkoder.spring.datajpa.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bezkoder.spring.datajpa.model.Estado_Tramite;
import com.bezkoder.spring.datajpa.repository.Estado_TramiteRepository;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api")
public class Estado_TramiteController {

	@Autowired
	Estado_TramiteRepository estado_tramiteRepository;

	@GetMapping("/estado_tramite")
	public ResponseEntity<List<Estado_Tramite>> getAllEstado_Tramite(@RequestParam(required = false) String nombre) {
		try {
			List<Estado_Tramite> estado_tramite = new ArrayList<Estado_Tramite>();

			if (nombre == null)
				estado_tramiteRepository.findAll().forEach(estado_tramite::add);
			else
				estado_tramiteRepository.findByNombreContaining(nombre).forEach(estado_tramite::add);

			if (estado_tramite.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(estado_tramite, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/estado_tramite/{id}")
	public ResponseEntity<Estado_Tramite> getEstado_SeguimientoById(@PathVariable("id") long id) {
		Optional<Estado_Tramite> Estado_TramiteData = estado_tramiteRepository.findById(id);

		if (Estado_TramiteData.isPresent()) {
			return new ResponseEntity<>(Estado_TramiteData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/estado_tramite")
	public ResponseEntity<Estado_Tramite> createEstado_tramite(@RequestBody Estado_Tramite estado_tramite) {
		try {
			Estado_Tramite _estado_tramite = estado_tramiteRepository
					
			.save(new Estado_Tramite(estado_tramite.getNombre(), estado_tramite.getDescripcion(), estado_tramite.getEstado(), estado_tramite.getFecha_alta(), estado_tramite.getFecha_baja(), estado_tramite.getFecha_modificacion(), estado_tramite.getUsuario_alta(), estado_tramite.getUsuario_baja(), estado_tramite.getUsuario_modificacion()));
			
			return new ResponseEntity<>(_estado_tramite, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping("/estado_tramite/{id}")
	public ResponseEntity<Estado_Tramite> updateEstado_Tramite(@PathVariable("id") long id, @RequestBody Estado_Tramite estado_tramite) {
		Optional<Estado_Tramite> estado_tramiteData = estado_tramiteRepository.findById(id);

		if (estado_tramiteData.isPresent()) {
			Estado_Tramite _estado_tramite = estado_tramiteData.get();
			_estado_tramite.setNombre(estado_tramite.getNombre());
			_estado_tramite.setDescripcion(estado_tramite.getDescripcion());
			return new ResponseEntity<>(estado_tramiteRepository.save(_estado_tramite), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/estado_tramite/{id}")
	public ResponseEntity<HttpStatus> deleteEstado_Tramite(@PathVariable("id") long id) {
		try {
			estado_tramiteRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/estado_tramite")
	public ResponseEntity<HttpStatus> deleteAllEstado_Tramite() {
		try {
			estado_tramiteRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	
}
