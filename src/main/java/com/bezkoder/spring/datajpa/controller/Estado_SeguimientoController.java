package com.bezkoder.spring.datajpa.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bezkoder.spring.datajpa.model.Estado_Seguimiento;
import com.bezkoder.spring.datajpa.repository.Estado_SeguimientoRepository;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api")
public class Estado_SeguimientoController {

	@Autowired
	Estado_SeguimientoRepository estado_seguimientoRepository;

	@GetMapping("/estado_seguimiento")
	public ResponseEntity<List<Estado_Seguimiento>> getAllEstado_Seguimiento(@RequestParam(required = false) String nombre) {
		try {
			List<Estado_Seguimiento> estado_seguimiento = new ArrayList<Estado_Seguimiento>();

			if (nombre == null)
				estado_seguimientoRepository.findAll().forEach(estado_seguimiento::add);
			else
				estado_seguimientoRepository.findByNombreContaining(nombre).forEach(estado_seguimiento::add);

			if (estado_seguimiento.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(estado_seguimiento, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/estado_seguimiento/{id}")
	public ResponseEntity<Estado_Seguimiento> getEstado_SeguimientoById(@PathVariable("id") long id) {
		Optional<Estado_Seguimiento> Estado_SeguimientoData = estado_seguimientoRepository.findById(id);

		if (Estado_SeguimientoData.isPresent()) {
			return new ResponseEntity<>(Estado_SeguimientoData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/estado_seguimiento")
	public ResponseEntity<Estado_Seguimiento> createDocumento(@RequestBody Estado_Seguimiento documento) {
		try {
			Estado_Seguimiento _estado_seguimiento = estado_seguimientoRepository
					
			.save(new Estado_Seguimiento(documento.getNombre(), documento.getDescripcion(), documento.getEstado(), documento.getFecha_alta(), documento.getFecha_baja(), documento.getFecha_modificacion(), documento.getUsuario_alta(), documento.getUsuario_baja(), documento.getUsuario_modificacion()));
			
			return new ResponseEntity<>(_estado_seguimiento, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping("/estado_seguimiento/{id}")
	public ResponseEntity<Estado_Seguimiento> updateEstado_Seguimiento(@PathVariable("id") long id, @RequestBody Estado_Seguimiento estado_seguimiento) {
		Optional<Estado_Seguimiento> estado_seguimientoData = estado_seguimientoRepository.findById(id);

		if (estado_seguimientoData.isPresent()) {
			Estado_Seguimiento _estado_seguimiento = estado_seguimientoData.get();
			_estado_seguimiento.setNombre(estado_seguimiento.getNombre());
			_estado_seguimiento.setDescripcion(estado_seguimiento.getDescripcion());
			return new ResponseEntity<>(estado_seguimientoRepository.save(_estado_seguimiento), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/estado_seguimiento/{id}")
	public ResponseEntity<HttpStatus> deleteEstado_Seguimiento(@PathVariable("id") long id) {
		try {
			estado_seguimientoRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/estado_seguimiento")
	public ResponseEntity<HttpStatus> deleteAllEstado_Seguimiento() {
		try {
			estado_seguimientoRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	
}
