package com.bezkoder.spring.datajpa.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bezkoder.spring.datajpa.model.Tramite;
import com.bezkoder.spring.datajpa.repository.TramiteRepository;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api")

public class TramiteController {

	@Autowired
	TramiteRepository tramiteRepository;

	@GetMapping("/tramite")
	public ResponseEntity<List<Tramite>> getAllTramite(@RequestParam(required = false) String nombre) {
		try {
			List<Tramite> tramite = new ArrayList<Tramite>();

			//if (nombre == null)
				tramiteRepository.findAll().forEach(tramite::add);
			//else
			//	tramiteRepository.findByNombreContaining(nombre).forEach(tarea::add);

			if (tramite.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(tramite, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/tramite/{id}")
	public ResponseEntity<Tramite> getTramiteById(@PathVariable("id") long id) {
		Optional<Tramite> TramiteData = tramiteRepository.findById(id);

		if (TramiteData.isPresent()) {
			return new ResponseEntity<>(TramiteData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/tramite")
	public ResponseEntity<Tramite> createProceso(@RequestBody Tramite tramite) {
		try {
			Tramite _tramite = tramiteRepository
					
			.save(new Tramite(tramite.getForma_tramite(), tramite.getIdseccion(), tramite.getIdproceso(), tramite.getPrioridad(), tramite.getIdestado_tramite(), tramite.getIdusuario_responsable(), tramite.getDescripcion(), tramite.getEstado(), tramite.getFecha_alta(), tramite.getFecha_baja(), tramite.getFecha_modificacion(), tramite.getUsuario_alta(), tramite.getUsuario_baja(), tramite.getUsuario_modificacion()));
			
			return new ResponseEntity<>(_tramite, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping("/tramite/{id}")
	public ResponseEntity<Tramite> updateTramite(@PathVariable("id") long id, @RequestBody Tramite tramite) {
		Optional<Tramite> tramiteData = tramiteRepository.findById(id);

		if (tramiteData.isPresent()) {
			Tramite _tramite = tramiteData.get();
			//_tramite.setNombre(tramite.getNombre());
			_tramite.setDescripcion(tramite.getDescripcion());
			return new ResponseEntity<>(tramiteRepository.save(_tramite), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/tramite/{id}")
	public ResponseEntity<HttpStatus> deleteTramite(@PathVariable("id") long id) {
		try {
			tramiteRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/tramite")
	public ResponseEntity<HttpStatus> deleteAllTramite() {
		try {
			tramiteRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
