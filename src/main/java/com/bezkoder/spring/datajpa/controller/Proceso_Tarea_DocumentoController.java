package com.bezkoder.spring.datajpa.controller;



import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bezkoder.spring.datajpa.model.Proceso_Tarea_Documento;
import com.bezkoder.spring.datajpa.repository.Proceso_Tarea_DocumentoRepository;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api")
public class Proceso_Tarea_DocumentoController {

	@Autowired
	Proceso_Tarea_DocumentoRepository proceso_tarea_documentoRepository;

	@GetMapping("/proceso_tarea_documento")
	public ResponseEntity<List<Proceso_Tarea_Documento>> getAllProceso_Tarea_Documento(@RequestParam(required = false) String usuario_alta) {
		try {
			List<Proceso_Tarea_Documento> proceso_tarea_documento = new ArrayList<Proceso_Tarea_Documento>();

			//if (nombre == null)
				proceso_tarea_documentoRepository.findAll().forEach(proceso_tarea_documento::add);
			//else
			//	proceso_tareaRepository.findByNombreContaining(nombre).forEach(estado_tramite::add);

			if (proceso_tarea_documento.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(proceso_tarea_documento, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	
	}

	@GetMapping("/proceso_tarea_documento/{id}")
	public ResponseEntity<Proceso_Tarea_Documento> getProceso_Tarea_DocumentoById(@PathVariable("id") long id) {
		Optional<Proceso_Tarea_Documento> Proceso_Tarea_DocumentoData = proceso_tarea_documentoRepository.findById(id);

		if (Proceso_Tarea_DocumentoData.isPresent()) {
			return new ResponseEntity<>(Proceso_Tarea_DocumentoData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/proceso_tarea_documento")
	public ResponseEntity<Proceso_Tarea_Documento> createProceso_Tarea_Documento(@RequestBody Proceso_Tarea_Documento proceso_tarea_documento) {
		try {
			Proceso_Tarea_Documento _proceso_tarea_documento = proceso_tarea_documentoRepository
					
			.save(new Proceso_Tarea_Documento(proceso_tarea_documento.getIdproceso(), proceso_tarea_documento.getIdtarea(), proceso_tarea_documento.getIddocumento(), proceso_tarea_documento.getEstado(), proceso_tarea_documento.getFecha_alta(), proceso_tarea_documento.getFecha_baja(), proceso_tarea_documento.getFecha_modificacion(), proceso_tarea_documento.getUsuario_alta(), proceso_tarea_documento.getUsuario_baja(), proceso_tarea_documento.getUsuario_modificacion()));
			
			return new ResponseEntity<>(_proceso_tarea_documento, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping("/proceso_tarea_documento/{id}")
	public ResponseEntity<Proceso_Tarea_Documento> updateProceso_Tarea_Documento(@PathVariable("id") long id, @RequestBody Proceso_Tarea_Documento proceso_tarea_documento) {
		Optional<Proceso_Tarea_Documento> proceso_tarea_documentoData = proceso_tarea_documentoRepository.findById(id);

		if (proceso_tarea_documentoData.isPresent()) {
			Proceso_Tarea_Documento _proceso_tarea_documento = proceso_tarea_documentoData.get();
			_proceso_tarea_documento.setUsuario_alta(proceso_tarea_documento.getUsuario_alta());
			_proceso_tarea_documento.setUsuario_baja(proceso_tarea_documento.getUsuario_baja());
			return new ResponseEntity<>(proceso_tarea_documentoRepository.save(_proceso_tarea_documento), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/proceso_tarea_documento/{id}")
	public ResponseEntity<HttpStatus> deleteProceso_Tarea_Documento(@PathVariable("id") long id) {
		try {
			proceso_tarea_documentoRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/proceso_tarea_documento")
	public ResponseEntity<HttpStatus> deleteAllProceso_Tarea_Documento() {
		try {
			proceso_tarea_documentoRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
}
