package com.bezkoder.spring.datajpa.model;

import java.io.Serializable;
import java.util.Objects;

public class Proceso_Tarea_UsuarioPK implements Serializable{
	private long idproceso;
    private long idtarea;
    private long idpersona;

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.idproceso);
        hash = 59 * hash + Objects.hashCode(this.idtarea);
        hash = 59 * hash + Objects.hashCode(this.idpersona);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Proceso_Tarea_UsuarioPK other = (Proceso_Tarea_UsuarioPK) obj;
        if (!Objects.equals(this.idproceso, other.idproceso)) {
            return false;
        }
        if (!Objects.equals(this.idtarea, other.idtarea)) {
            return false;
        }
        return true;
    }
}
