package com.bezkoder.spring.datajpa.model;


import java.util.Date;

import javax.persistence.*;
// Autor José Luis
// Fecha 29/07/2022

@Entity
@Table(name = "wfl_seguimiento", schema="wfl")
public class Seguimiento {

	@Id
	@SequenceGenerator(schema="wfl",name="wfl_seguimiento",sequenceName="wfl_seguimiento",allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,  generator="wfl_seguimiento_idseguimiento_seq")
	@Column(name = "idseguimiento", updatable=false)	
	
	private long idtramite;
	private long idtarea;
	private int accion_realizada;
	private long idestado_seguimiento;
	private Date fecha_inicio;
	private Date fecha_fin;
	private long idseguimiento_anterior;
	private String idusuario_responsable;
	private int tiempo_espera;
	private int tiempo_atencion;
	private String estado;
	private Date fecha_alta;
	private Date fecha_baja;
	private Date fecha_modificacion;
	private String usuario_alta;
	private String usuario_baja;
	private String usuario_modificacion;

	
	public Seguimiento () {
		
		
	}

	public Seguimiento(long idtramite, long idtarea, int accion_realizada, long idestado_seguimiento, Date fecha_inicio,
			Date fecha_fin, long idseguimiento_anterior, String idusuario_responsable, int tiempo_espera,
			int tiempo_atencion, String estado, Date fecha_alta, Date fecha_baja, Date fecha_modificacion,
			String usuario_alta, String usuario_baja, String usuario_modificacion) {
		super();
		this.idtramite = idtramite;
		this.idtarea = idtarea;
		this.accion_realizada = accion_realizada;
		this.idestado_seguimiento = idestado_seguimiento;
		this.fecha_inicio = fecha_inicio;
		this.fecha_fin = fecha_fin;
		this.idseguimiento_anterior = idseguimiento_anterior;
		this.idusuario_responsable = idusuario_responsable;
		this.tiempo_espera = tiempo_espera;
		this.tiempo_atencion = tiempo_atencion;
		this.estado = estado;
		this.fecha_alta = fecha_alta;
		this.fecha_baja = fecha_baja;
		this.fecha_modificacion = fecha_modificacion;
		this.usuario_alta = usuario_alta;
		this.usuario_baja = usuario_baja;
		this.usuario_modificacion = usuario_modificacion;
	}
	public long getIdtramite() {
		return idtramite;
	}
	public void setIdtramite(long idtramite) {
		this.idtramite = idtramite;
	}
	public long getIdtarea() {
		return idtarea;
	}
	public void setIdtarea(long idtarea) {
		this.idtarea = idtarea;
	}
	public int getAccion_realizada() {
		return accion_realizada;
	}
	public void setAccion_realizada(int accion_realizada) {
		this.accion_realizada = accion_realizada;
	}
	public long getIdestado_seguimiento() {
		return idestado_seguimiento;
	}
	public void setIdestado_seguimiento(long idestado_seguimiento) {
		this.idestado_seguimiento = idestado_seguimiento;
	}
	public Date getFecha_inicio() {
		return fecha_inicio;
	}
	public void setFecha_inicio(Date fecha_inicio) {
		this.fecha_inicio = fecha_inicio;
	}
	public Date getFecha_fin() {
		return fecha_fin;
	}
	public void setFecha_fin(Date fecha_fin) {
		this.fecha_fin = fecha_fin;
	}
	public long getIdseguimiento_anterior() {
		return idseguimiento_anterior;
	}
	public void setIdseguimiento_anterior(long idseguimiento_anterior) {
		this.idseguimiento_anterior = idseguimiento_anterior;
	}
	public String getIdusuario_responsable() {
		return idusuario_responsable;
	}
	public void setIdusuario_responsable(String idusuario_responsable) {
		this.idusuario_responsable = idusuario_responsable;
	}
	public int getTiempo_espera() {
		return tiempo_espera;
	}
	public void setTiempo_espera(int tiempo_espera) {
		this.tiempo_espera = tiempo_espera;
	}
	public int getTiempo_atencion() {
		return tiempo_atencion;
	}
	public void setTiempo_atencion(int tiempo_atencion) {
		this.tiempo_atencion = tiempo_atencion;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Date getFecha_alta() {
		return fecha_alta;
	}
	public void setFecha_alta(Date fecha_alta) {
		this.fecha_alta = fecha_alta;
	}
	public Date getFecha_baja() {
		return fecha_baja;
	}
	public void setFecha_baja(Date fecha_baja) {
		this.fecha_baja = fecha_baja;
	}
	public Date getFecha_modificacion() {
		return fecha_modificacion;
	}
	public void setFecha_modificacion(Date fecha_modificacion) {
		this.fecha_modificacion = fecha_modificacion;
	}
	public String getUsuario_alta() {
		return usuario_alta;
	}
	public void setUsuario_alta(String usuario_alta) {
		this.usuario_alta = usuario_alta;
	}
	public String getUsuario_baja() {
		return usuario_baja;
	}
	public void setUsuario_baja(String usuario_baja) {
		this.usuario_baja = usuario_baja;
	}
	public String getUsuario_modificacion() {
		return usuario_modificacion;
	}
	public void setUsuario_modificacion(String usuario_modificacion) {
		this.usuario_modificacion = usuario_modificacion;
	}
	
}