package com.bezkoder.spring.datajpa.model;

import java.io.Serializable;
import java.util.Objects;

public class Proceso_TareaPK implements Serializable{
	private long idproceso;
    private long idtarea;

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.idproceso);
        hash = 59 * hash + Objects.hashCode(this.idtarea);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Proceso_TareaPK other = (Proceso_TareaPK) obj;
        if (!Objects.equals(this.idproceso, other.idproceso)) {
            return false;
        }
        if (!Objects.equals(this.idtarea, other.idtarea)) {
            return false;
        }
        return true;
    }
}
