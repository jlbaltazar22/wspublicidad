package com.bezkoder.spring.datajpa.model;

import java.util.Date;

import javax.persistence.*;
// Autor José Luis
// Fecha 02/08/2022

@Entity
@Table(name = "wfl_tarea", schema="wfl")
public class Tarea {

	@Id
	@SequenceGenerator(schema="wfl",name="wfl_tarea_idtarea_seq",sequenceName="wfl_tarea_idtarea_seq",allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,  generator="wfl_tarea_idtarea_seq")
	@Column(name = "idtarea", updatable=false)	
	private long idtarea;
	private String notificacion_correo;
	private String requiere_autorizacion;
	private String notificacion_autorizador;
	private int idtipo_autorizacion;
	private String url_formulario;
	private long idproceso;
	private String estado;
	private Date fecha_alta;
	private Date fecha_baja;
	private Date fecha_modificacion;
	private String usuario_alta;
	private String usuario_baja;
	private String usuario_modificacion;
	private String nombre;
	private String descripcion;
	private int duracion_estandar;
	private int duracion_media;
	private int duracion_alta;
	
	public Tarea() {

	}

	public Tarea(String notificacion_correo, String requiere_autorizacion, String notificacion_autorizador,
			int idtipo_autorizacion, String url_formulario, long idproceso, String estado, Date fecha_alta,
			Date fecha_baja, Date fecha_modificacion, String usuario_alta, String usuario_baja,
			String usuario_modificacion, String nombre, String descripcion, int duracion_estandar, int duracion_media,
			int duracion_alta) {
		super();
		this.notificacion_correo = notificacion_correo;
		this.requiere_autorizacion = requiere_autorizacion;
		this.notificacion_autorizador = notificacion_autorizador;
		this.idtipo_autorizacion = idtipo_autorizacion;
		this.url_formulario = url_formulario;
		this.idproceso = idproceso;
		this.estado = estado;
		this.fecha_alta = fecha_alta;
		this.fecha_baja = fecha_baja;
		this.fecha_modificacion = fecha_modificacion;
		this.usuario_alta = usuario_alta;
		this.usuario_baja = usuario_baja;
		this.usuario_modificacion = usuario_modificacion;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.duracion_estandar = duracion_estandar;
		this.duracion_media = duracion_media;
		this.duracion_alta = duracion_alta;
	}

	public long getIdtarea() {
		return idtarea;
	}

	public void setIdtarea(long idtarea) {
		this.idtarea = idtarea;
	}

	public String getNotificacion_correo() {
		return notificacion_correo;
	}

	public void setNotificacion_correo(String notificacion_correo) {
		this.notificacion_correo = notificacion_correo;
	}

	public String getRequiere_autorizacion() {
		return requiere_autorizacion;
	}

	public void setRequiere_autorizacion(String requiere_autorizacion) {
		this.requiere_autorizacion = requiere_autorizacion;
	}

	public String getNotificacion_autorizador() {
		return notificacion_autorizador;
	}

	public void setNotificacion_autorizador(String notificacion_autorizador) {
		this.notificacion_autorizador = notificacion_autorizador;
	}

	public int getIdtipo_autorizacion() {
		return idtipo_autorizacion;
	}

	public void setIdtipo_autorizacion(int idtipo_autorizacion) {
		this.idtipo_autorizacion = idtipo_autorizacion;
	}

	public String getUrl_formulario() {
		return url_formulario;
	}

	public void setUrl_formulario(String url_formulario) {
		this.url_formulario = url_formulario;
	}

	public long getIdproceso() {
		return idproceso;
	}

	public void setIdproceso(long idproceso) {
		this.idproceso = idproceso;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFecha_alta() {
		return fecha_alta;
	}

	public void setFecha_alta(Date fecha_alta) {
		this.fecha_alta = fecha_alta;
	}

	public Date getFecha_baja() {
		return fecha_baja;
	}

	public void setFecha_baja(Date fecha_baja) {
		this.fecha_baja = fecha_baja;
	}

	public Date getFecha_modificacion() {
		return fecha_modificacion;
	}

	public void setFecha_modificacion(Date fecha_modificacion) {
		this.fecha_modificacion = fecha_modificacion;
	}

	public String getUsuario_alta() {
		return usuario_alta;
	}

	public void setUsuario_alta(String usuario_alta) {
		this.usuario_alta = usuario_alta;
	}

	public String getUsuario_baja() {
		return usuario_baja;
	}

	public void setUsuario_baja(String usuario_baja) {
		this.usuario_baja = usuario_baja;
	}

	public String getUsuario_modificacion() {
		return usuario_modificacion;
	}

	public void setUsuario_modificacion(String usuario_modificacion) {
		this.usuario_modificacion = usuario_modificacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getDuracion_estandar() {
		return duracion_estandar;
	}

	public void setDuracion_estandar(int duracion_estandar) {
		this.duracion_estandar = duracion_estandar;
	}

	public int getDuracion_media() {
		return duracion_media;
	}

	public void setDuracion_media(int duracion_media) {
		this.duracion_media = duracion_media;
	}

	public int getDuracion_alta() {
		return duracion_alta;
	}

	public void setDuracion_alta(int duracion_alta) {
		this.duracion_alta = duracion_alta;
	}

}
