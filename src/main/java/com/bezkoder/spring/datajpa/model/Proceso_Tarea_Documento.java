package com.bezkoder.spring.datajpa.model;

import java.util.Date;

import javax.persistence.*;
// Autor José Luis
// Fecha 29/07/2022

@Entity
@Table(name = "wfl_proceso_tarea_documento", schema="wfl")
@IdClass(value = Proceso_Tarea_DocumentoPK.class)
public class Proceso_Tarea_Documento {
	@Id
	private long idproceso;
	@Id
	private long idtarea;
	@Id
	private long iddocumento;
	
	private String es_requerido;
	private String estado;
	private Date fecha_alta;
	private Date fecha_baja;
	private Date fecha_modificacion;
	private String usuario_alta;
	private String usuario_baja;
	private String usuario_modificacion;
	
	public Proceso_Tarea_Documento() {

	}

	public Proceso_Tarea_Documento(long idproceso, long idtarea, long iddocumento, String estado, Date fecha_alta, Date fecha_baja,
			Date fecha_modificacion, String usuario_alta, String usuario_baja, String usuario_modificacion) {
		super();
		this.idproceso = idproceso;
		this.idtarea = idtarea;
		this.iddocumento= iddocumento;
		this.estado = estado;
		this.fecha_alta = fecha_alta;
		this.fecha_baja = fecha_baja;
		this.fecha_modificacion = fecha_modificacion;
		this.usuario_alta = usuario_alta;
		this.usuario_baja = usuario_baja;
		this.usuario_modificacion = usuario_modificacion;
	}

	public long getIdproceso() {
		return idproceso;
	}

	public void setIdproceso(long idproceso) {
		this.idproceso = idproceso;
	}

	public long getIdtarea() {
		return idtarea;
	}

	public void setIdtarea(long idtarea) {
		this.idtarea = idtarea;
	}

	public long getIddocumento() {
		return iddocumento;
	}

	public void setIddocumento(long iddocumento) {
		this.iddocumento = iddocumento;
	}

	public String getEs_requerido() {
		return es_requerido;
	}

	public void setEs_requerido(String es_requerido) {
		this.es_requerido = es_requerido;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFecha_alta() {
		return fecha_alta;
	}

	public void setFecha_alta(Date fecha_alta) {
		this.fecha_alta = fecha_alta;
	}

	public Date getFecha_baja() {
		return fecha_baja;
	}

	public void setFecha_baja(Date fecha_baja) {
		this.fecha_baja = fecha_baja;
	}

	public Date getFecha_modificacion() {
		return fecha_modificacion;
	}

	public void setFecha_modificacion(Date fecha_modificacion) {
		this.fecha_modificacion = fecha_modificacion;
	}

	public String getUsuario_alta() {
		return usuario_alta;
	}

	public void setUsuario_alta(String usuario_alta) {
		this.usuario_alta = usuario_alta;
	}

	public String getUsuario_baja() {
		return usuario_baja;
	}

	public void setUsuario_baja(String usuario_baja) {
		this.usuario_baja = usuario_baja;
	}

	public String getUsuario_modificacion() {
		return usuario_modificacion;
	}

	public void setUsuario_modificacion(String usuario_modificacion) {
		this.usuario_modificacion = usuario_modificacion;
	}


}
