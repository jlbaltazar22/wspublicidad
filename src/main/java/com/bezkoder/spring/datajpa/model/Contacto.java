package com.bezkoder.spring.datajpa.model;

import javax.persistence.*;
//Autor Marcelo
//Fecha 16/05/2021



@Entity
@Table(name = "contacto", schema="public")
public class Contacto {

	@Id
	@SequenceGenerator(schema="public",name="tutorials_id_seq",sequenceName="tutorials_id_seq",allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,  generator="tutorials_id_seq")
	@Column(name = "idcontacto", updatable=false)	
	private long idcontacto;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "direccion")
	private String direccion;

	public Contacto() {

	}

	public Contacto(String nombre, String direccion) {
		super();
		this.nombre = nombre;
		this.direccion = direccion;
	}

	public long getIdcontacto() {
		return idcontacto;
	}

	public void setIdcontacto(long idsucursal) {
		this.idcontacto = idsucursal;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
}
