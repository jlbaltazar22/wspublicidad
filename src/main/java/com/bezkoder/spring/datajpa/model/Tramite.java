package com.bezkoder.spring.datajpa.model;


import java.util.Date;

import javax.persistence.*;
// Autor José Luis
// Fecha 03/08/2022

@Entity
@Table(name = "wfl_tramite", schema="wfl")
public class Tramite {

	@Id
	@SequenceGenerator(schema="wfl",name="wfl_tramite_idtramite_seq",sequenceName="wfl_tramite_idtramite_seq",allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,  generator="wfl_tramite_idtramite_seq")
	@Column(name = "idtramite", updatable=false)	
	private long idtramite;
	private String forma_tramite;
	private long idseccion;
	private long idproceso;
	private int prioridad;
	private int idestado_tramite;
	private int idusuario_responsable;
	private String descripcion;
	private String estado;
	private Date fecha_alta;
	private Date fecha_baja;
	private Date fecha_modificacion;
	private String usuario_alta;
	private String usuario_baja;
	private String usuario_modificacion;
	
	public Tramite() {

	}

	public Tramite(String forma_tramite, long idseccion, long idproceso, int prioridad, int idestado_tramite,
			int idusuario_responsable, String descripcion, String estado, Date fecha_alta, Date fecha_baja,
			Date fecha_modificacion, String usuario_alta, String usuario_baja, String usuario_modificacion) {
		super();
		this.forma_tramite = forma_tramite;
		this.idseccion = idseccion;
		this.idproceso = idproceso;
		this.prioridad = prioridad;
		this.idestado_tramite = idestado_tramite;
		this.idusuario_responsable = idusuario_responsable;
		this.descripcion = descripcion;
		this.estado = estado;
		this.fecha_alta = fecha_alta;
		this.fecha_baja = fecha_baja;
		this.fecha_modificacion = fecha_modificacion;
		this.usuario_alta = usuario_alta;
		this.usuario_baja = usuario_baja;
		this.usuario_modificacion = usuario_modificacion;
	}

	public long getIdtramite() {
		return idtramite;
	}

	public void setIdtramite(long idtramite) {
		this.idtramite = idtramite;
	}

	public String getForma_tramite() {
		return forma_tramite;
	}

	public void setForma_tramite(String forma_tramite) {
		this.forma_tramite = forma_tramite;
	}

	public long getIdseccion() {
		return idseccion;
	}

	public void setIdseccion(long idseccion) {
		this.idseccion = idseccion;
	}

	public long getIdproceso() {
		return idproceso;
	}

	public void setIdproceso(long idproceso) {
		this.idproceso = idproceso;
	}

	public int getPrioridad() {
		return prioridad;
	}

	public void setPrioridad(int prioridad) {
		this.prioridad = prioridad;
	}

	public int getIdestado_tramite() {
		return idestado_tramite;
	}

	public void setIdestado_tramite(int idestado_tramite) {
		this.idestado_tramite = idestado_tramite;
	}

	public int getIdusuario_responsable() {
		return idusuario_responsable;
	}

	public void setIdusuario_responsable(int idusuario_responsable) {
		this.idusuario_responsable = idusuario_responsable;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFecha_alta() {
		return fecha_alta;
	}

	public void setFecha_alta(Date fecha_alta) {
		this.fecha_alta = fecha_alta;
	}

	public Date getFecha_baja() {
		return fecha_baja;
	}

	public void setFecha_baja(Date fecha_baja) {
		this.fecha_baja = fecha_baja;
	}

	public Date getFecha_modificacion() {
		return fecha_modificacion;
	}

	public void setFecha_modificacion(Date fecha_modificacion) {
		this.fecha_modificacion = fecha_modificacion;
	}

	public String getUsuario_alta() {
		return usuario_alta;
	}

	public void setUsuario_alta(String usuario_alta) {
		this.usuario_alta = usuario_alta;
	}

	public String getUsuario_baja() {
		return usuario_baja;
	}

	public void setUsuario_baja(String usuario_baja) {
		this.usuario_baja = usuario_baja;
	}

	public String getUsuario_modificacion() {
		return usuario_modificacion;
	}

	public void setUsuario_modificacion(String usuario_modificacion) {
		this.usuario_modificacion = usuario_modificacion;
	}

}
