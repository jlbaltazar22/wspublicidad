package com.bezkoder.spring.datajpa.model;

import java.util.Date;

import javax.persistence.*;
// Autor José Luis
// Fecha 29/07/2022

@Entity
@Table(name = "wfl_seccion", schema="wfl")
public class Seccion {

	@Id
	@SequenceGenerator(schema="wfl",name="wfl_seccion_idseccion_seq",sequenceName="wfl_seccion_idseccion_seq",allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,  generator="wfl_seccion_idseccion_seq")
	@Column(name = "idseccion", updatable=false)	
	private long idseccion;
	private String nombre;
	private String descripcion;
	private String estado;
	private Date fecha_alta;
	private Date fecha_baja;
	private Date fecha_modificacion;
	private String usuario_alta;
	private String usuario_baja;
	private String usuario_modificacion;
	
	public Seccion() {

	}

	public Seccion(long idseccion, String nombre, String descripcion, String estado, Date fecha_alta, Date fecha_baja,
			Date fecha_modificacion, String usuario_alta, String usuario_baja, String usuario_modificacion) {
		super();
		this.idseccion=idseccion;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.estado = estado;
		this.fecha_alta = fecha_alta;
		this.fecha_baja = fecha_baja;
		this.fecha_modificacion = fecha_modificacion;
		this.usuario_alta = usuario_alta;
		this.usuario_baja = usuario_baja;
		this.usuario_modificacion = usuario_modificacion;
	}

	public long getIdseccion() {
		return idseccion;
	}

	public void setIdseccion(long idseccion) {
		this.idseccion = idseccion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFecha_alta() {
		return fecha_alta;
	}

	public void setFecha_alta(Date fecha_alta) {
		this.fecha_alta = fecha_alta;
	}

	public Date getFecha_baja() {
		return fecha_baja;
	}

	public void setFecha_baja(Date fecha_baja) {
		this.fecha_baja = fecha_baja;
	}

	public Date getFecha_modificacion() {
		return fecha_modificacion;
	}

	public void setFecha_modificacion(Date fecha_modificacion) {
		this.fecha_modificacion = fecha_modificacion;
	}

	public String getUsuario_alta() {
		return usuario_alta;
	}

	public void setUsuario_alta(String usuario_alta) {
		this.usuario_alta = usuario_alta;
	}

	public String getUsuario_baja() {
		return usuario_baja;
	}

	public void setUsuario_baja(String usuario_baja) {
		this.usuario_baja = usuario_baja;
	}

	public String getUsuario_modificacion() {
		return usuario_modificacion;
	}

	public void setUsuario_modificacion(String usuario_modificacion) {
		this.usuario_modificacion = usuario_modificacion;
	}

}
