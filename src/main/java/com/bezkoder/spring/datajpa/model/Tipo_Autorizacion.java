package com.bezkoder.spring.datajpa.model;


import java.util.Date;

import javax.persistence.*;
// Autor José Luis
// Fecha 29/07/2022

@Entity
@Table(name = "wfl_tipo_autorizacion", schema="wfl")
public class Tipo_Autorizacion {

	@Id
	@SequenceGenerator(schema="wfl",name="wfl_tipo_autorizacion_idtipo_autorizacion_seq",sequenceName="wfl_tipo_autorizacion_idtipo_autorizacion_seq",allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,  generator="wfl_tipo_autorizacion_idtipo_autorizacion_seq")
	@Column(name = "idtipo_autorizacion", updatable=false)	
	private long idtipo_autorizacion;
	private String nombre;
	private String descripcion;
	private String estado;
	private Date fecha_alta;
	private Date fecha_baja;
	private Date fecha_modificacion;
	private String usuario_alta;
	private String usuario_baja;
	private String usuario_modificacion;
	
	public Tipo_Autorizacion() {

	}

	public Tipo_Autorizacion(long idtipo_autorizacion, String nombre, String descripcion, String estado, Date fecha_alta, Date fecha_baja,
			Date fecha_modificacion, String usuario_alta, String usuario_baja, String usuario_modificacion) {
		super();
		this.idtipo_autorizacion=idtipo_autorizacion;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.estado = estado;
		this.fecha_alta = fecha_alta;
		this.fecha_baja = fecha_baja;
		this.fecha_modificacion = fecha_modificacion;
		this.usuario_alta = usuario_alta;
		this.usuario_baja = usuario_baja;
		this.usuario_modificacion = usuario_modificacion;
	}

	public long getIdtipo_autorizacion() {
		return idtipo_autorizacion;
	}

	public void setIdtipo_autorizacion(long idtipo_autorizacion) {
		this.idtipo_autorizacion = idtipo_autorizacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFecha_alta() {
		return fecha_alta;
	}

	public void setFecha_alta(Date fecha_alta) {
		this.fecha_alta = fecha_alta;
	}

	public Date getFecha_baja() {
		return fecha_baja;
	}

	public void setFecha_baja(Date fecha_baja) {
		this.fecha_baja = fecha_baja;
	}

	public Date getFecha_modificacion() {
		return fecha_modificacion;
	}

	public void setFecha_modificacion(Date fecha_modificacion) {
		this.fecha_modificacion = fecha_modificacion;
	}

	public String getUsuario_alta() {
		return usuario_alta;
	}

	public void setUsuario_alta(String usuario_alta) {
		this.usuario_alta = usuario_alta;
	}

	public String getUsuario_baja() {
		return usuario_baja;
	}

	public void setUsuario_baja(String usuario_baja) {
		this.usuario_baja = usuario_baja;
	}

	public String getUsuario_modificacion() {
		return usuario_modificacion;
	}

	public void setUsuario_modificacion(String usuario_modificacion) {
		this.usuario_modificacion = usuario_modificacion;
	}
	
}
