package com.bezkoder.spring.datajpa.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bezkoder.spring.datajpa.model.Documento;

public interface DocumentoRepository extends JpaRepository<Documento, Long> {
	//List<Sucursal> findByPublished(boolean published);
	List<Documento> findByNombreContaining(String nombre);
	List<Documento> findByDescripcionContaining(String descripcion);
}
