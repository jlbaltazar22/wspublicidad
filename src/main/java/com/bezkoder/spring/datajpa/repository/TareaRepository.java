package com.bezkoder.spring.datajpa.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bezkoder.spring.datajpa.model.Tarea;

public interface TareaRepository extends JpaRepository<Tarea, Long> {
	
	List<Tarea> findByNombreContaining(String nombre);
	List<Tarea> findByDescripcionContaining(String descripcion);
	
}
