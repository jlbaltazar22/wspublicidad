package com.bezkoder.spring.datajpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bezkoder.spring.datajpa.model.Contacto;

public interface ContactoRepository extends JpaRepository<Contacto, Long> {
	//List<Contacto> findByPublished(boolean published);
	List<Contacto> findByNombreContaining(String title);
}
