package com.bezkoder.spring.datajpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bezkoder.spring.datajpa.model.Proceso_Tarea_Documento;

public interface Proceso_Tarea_DocumentoRepository extends JpaRepository<Proceso_Tarea_Documento, Long> {
	
	//List<Proceso_Tarea> findByNombreContaining(String nombre);
	//List<Proceso_Tarea> findByDescripcionContaining(String descripcion);
}
