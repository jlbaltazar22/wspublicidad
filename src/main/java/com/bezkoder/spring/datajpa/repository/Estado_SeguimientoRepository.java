package com.bezkoder.spring.datajpa.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bezkoder.spring.datajpa.model.Estado_Seguimiento;

public interface Estado_SeguimientoRepository extends JpaRepository<Estado_Seguimiento, Long> {
	
	List<Estado_Seguimiento> findByNombreContaining(String nombre);
	List<Estado_Seguimiento> findByDescripcionContaining(String descripcion);
}
