package com.bezkoder.spring.datajpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bezkoder.spring.datajpa.model.Seguimiento;

public interface SeguimientoRepository extends JpaRepository<Seguimiento, Long> {
	
	//List<Seguimiento> findByNombreContaining(String nombre);
	//List<Seguimiento> findByUsuario_altaContaining(String usuario_alta);
}
