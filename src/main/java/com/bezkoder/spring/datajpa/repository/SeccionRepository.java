package com.bezkoder.spring.datajpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bezkoder.spring.datajpa.model.Seccion;

public interface SeccionRepository extends JpaRepository<Seccion, Long> {
	
	List<Seccion> findByNombreContaining(String nombre);
	List<Seccion> findByDescripcionContaining(String descripcion);
}
