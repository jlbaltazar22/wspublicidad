package com.bezkoder.spring.datajpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bezkoder.spring.datajpa.model.Proceso;

public interface ProcesoRepository extends JpaRepository<Proceso, Long> {
	
	List<Proceso> findByNombreContaining(String nombre);
	List<Proceso> findByDescripcionContaining(String descripcion);
}
