package com.bezkoder.spring.datajpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bezkoder.spring.datajpa.model.Tipo_Autorizacion;

public interface Tipo_AutorizacionRepository extends JpaRepository<Tipo_Autorizacion, Long> {
	
	List<Tipo_Autorizacion> findByNombreContaining(String nombre);
	List<Tipo_Autorizacion> findByDescripcionContaining(String descripcion);
}
