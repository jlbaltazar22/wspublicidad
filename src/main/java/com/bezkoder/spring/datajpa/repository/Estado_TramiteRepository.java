package com.bezkoder.spring.datajpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bezkoder.spring.datajpa.model.Estado_Tramite;

public interface Estado_TramiteRepository extends JpaRepository<Estado_Tramite, Long> {
	
	List<Estado_Tramite> findByNombreContaining(String nombre);
	List<Estado_Tramite> findByDescripcionContaining(String descripcion);
}
